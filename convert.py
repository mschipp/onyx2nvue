# Start date 19/07/2021
# Convert Onyx configs into CL configs
# Version 0.1 / Dated 19/07/2021 / bad coding by Michael Schipp
# Version 0.11 / Dated 21/07/2021 / Added PTP


import sys, getopt, re
#set global veriable
current_ver = '0.11'
CUE = "nv"
inputfile = ''
outputfile = 'output.txt'

def main(argv):   
   if not argv:     #check if no arg are entered
      print ('Onux2CL.py -i <inputfile> -o <outputfile> -c')
      print ('')
      print ('Usage:')
      print ('Input file must be specificed by using -i')
      print ('If -o is not specified then dafault of output.txt is used')
      print ('Optional -c will use cl set instead of the default of nv set')
      sys.exit()    # no arg, bugging out of here
   global inputfile,outputfile,CUE  #use gloabal veriable
   try:
      opts, args = getopt.getopt(argv,"hi:o:c",["ifile=","ofile="])
   except getopt.GetoptError:
      print ('Onux2CL.py -i <inputfile> -o <outputfile> -c')
      print ('')
      print ('Usage:')
      print ('If -o is not specified then dafault of output.txt is used')
      print ('-c will use cl set, default is to use nv set')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('Onux2CL.py -i <inputfile> -o <outputfile> -c')
         print ('')
         print ('Usage:')
         print ('If -o is not specified then dafault of output.txt is used')
         print ('-c will use cl set, default is to use nv set')
         
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt == '-c':
         CUE = "cl"         
  
if __name__ == "__main__":
   main(sys.argv[1:])
line_output = []
in_data2 = []
line_str = ""
temp_list = []
temp_str = ""
write_line = True
input_file = open(inputfile,'r')            # open the file for raeading only
in_data = input_file.readlines()            # read the file into a list
ouput_file = open(outputfile,'w')           # open the file for writting to 

title = '#Converted from Onyx to Cumulus Linux\n'    # add comment to file to say what created this file
ouput_file.write(title)

# first let change interface format from 1/x to swpx
for line_data in in_data:
    line_str = line_data
    if line_str.find('interface ethernet 1/') != -1:
       line_str = line_str.replace('1/', 'swp')
    in_data2.append(line_str)

for line_data in in_data2:
    line_str = line_data.lstrip()
    if line_str.startswith('traffic pool roce type lossless'):
       line_str = line_str.replace('traffic pool roce type lossless', CUE +' set qos roce mode lossless')
    elif line_str == 'interface loopback 0\n': #look for loopback defs
       line_str = '' #not needed so remove it           
    elif (line_str.startswith('interface loopback ') and line_str.find('ospf area') != -1): #look for
       line_str = '' #not needed so remove it
    elif line_str.find('no switchport force') != -1: #look for changing onyx switchport to router port
       line_str = '' #not needed so remove it
    elif line_str.startswith('#'): #look for comment lines
       line_str = '' #not needed so remove it
    elif (line_str.startswith('interface ethernet') and line_str.find('ip address') != -1): #look for interface ip addressing
       temp_list = line_str.split() #temp list to hold each word
       line_str = line_str.replace('primary','')
       line_str = line_str.replace('interface ethernet', CUE +' set interface')
    elif (line_str.startswith('interface ethernet') and line_str.find('speed') != -1): #look for interface with speed
       temp_list = line_str.split() #temp list to hold each word     
       line_str = line_str.replace('interface ethernet', CUE +' set interface')
       line_str = line_str.replace('force','')
       line_str = line_str.replace('speed', 'link speed')
    elif line_str.startswith('hostname'): 
       line_str = line_str.replace('hostname ', CUE +' set platform hostname ')
    elif (line_str.startswith('interface vlan') and line_str.find('ipl') == -1):
       line_str = line_str.replace('interface vlan ', CUE +' set interface vlan')
       line_str = line_str.replace('primary','')
    elif line_str.startswith('interface loopback'): 
       line_str = line_str.replace('interface loopback 0', CUE +' set interface lo')
       line_str = line_str.replace('primary','')
    elif line_str.startswith('protocol ospf'):
       line_str = line_str.replace('protocol ospf', CUE +' set router ospf enable on')
    elif line_str.startswith('router ospf 1 vrf'):
       line_str = line_str.replace('router ospf 1 vrf default', CUE +' set vrf default router ospf')
    elif (line_str.startswith('interface ethernet ') and line_str.find('ospf area') != -1): #look for
       line_str = line_str.replace('interface ethernet', CUE +' set interface')
       line_str = line_str.replace(' ip', '')
    elif line_str.startswith('no spanning-tree'):
       line_str = line_str.replace('no spanning-tree', CUE +' set bridge domain br stp state down')
    elif line_str.startswith('ntp server'):
       line_str = line_str.replace('ntp server', CUE +' set system ntp')
    elif line_str == 'protocol bgp\n':
       line_str = CUE +' set router bgp enable on\n'
    elif line_str == 'protocol ptp\n':
       line_str = CUE +' set service ptp 1 enable on\n'
    elif (line_str.startswith('interface ethernet ') and line_str.find('enable forced-master') != -1): #look for
       line_str = line_str.replace('interface ethernet', CUE +' set interface')
       line_str = line_str.replace('enable forced-master', 'forced-master on')
    elif (line_str.startswith('interface ethernet ') and line_str.find('description') != -1): #look for
       line_str = line_str.replace('\n', ' <-- NVUE has no command for this yet\n')
    elif (line_str.startswith('interface ethernet ') and line_str.find('ptp enable') != -1): #look for
       line_str = line_str.replace('interface ethernet', CUE +' set interface')
       line_str = line_str.replace('ptp enable', 'ptp enable on')
    elif line_str.startswith('ptp amt'): #look for
       line_str = line_str.replace('ptp amt', CUE +' set service ptp 1 acceptable-master')
    elif (line_str.startswith('interface mlag-port-channel') and line_str.find('switchport trunk allowed-vlan add ') != -1):
       temp_list = line_str.split() #temp list to hold each word
       line_str = CUE +' set interface bond' +temp_list[2] + ' bridge domain br_default vlan ' + temp_list[-1] +'\n'
    elif (line_str.startswith('interface mlag-port-channel') and line_str.find('mtu') != -1):
       temp_list = line_str.split() #temp list to hold each word
       line_str = CUE +' set interface bond' +temp_list[2] + ' link mtu ' + temp_list[4] + '\n'
    elif (line_str.startswith('interface mlag-port-channel') and line_str.find('no shutdown') != -1):
       temp_list = line_str.split() #temp list to hold each word
       line_str = CUE +' set interface bond' +temp_list[2] + ' link state up\n'
    elif (line_str.startswith('interface mlag-port-channel') and line_str.find('description') != -1):
       line_str = line_str.replace('\n',' <-- NVUE has no command for this yet\n')
    elif line_str.startswith('interface mlag-port-channel'):
       temp_list = line_str.split() #temp list to hold each word
       line_str = line_str.replace('interface mlag-port-channel ', CUE +' set interface bond')
       line_str = line_str.replace('\n', '')
       line_str = line_str + ' bond mlag id ' + temp_list[2] +'\n'
    elif (line_str.startswith('interface ethernet') and line_str.find('mlag-channel-group') != -1):
       temp_list = line_str.split() #temp list to hold each word
       line_str = CUE +' set interface ' +temp_list[4] + ' bond memeber ' + temp_list[2] + '\n'
    elif line_str.startswith('mlag system-mac'):
       line_str = line_str.replace('mlag system-mac ', CUE +' set mlag mac-address ')
    elif (line_str.startswith('interface vlan') and line_str.find('ipl') != -1):
       line_str = CUE +' set mlag peer-ip linklocal\n'
    #elif (line_str.startswith('interface port-channel') and line_str.find('ipl') != -1):
    #   line_str = CUE +' set mlag peer-ip linklocal\n'
    if write_line:
       line_output.extend(line_str)
       #print (line_str)
    else:
       write_line = True
    
for i in line_output:                       # go thouw line by line of the input file
    ouput_file.write(i)                     # write new output file
    #print(i)                               # dump it on the screen too
input_file.close()                          # close the input file
ouput_file.close()                          # close the output file

print ('Convert Onyx to Cumulus Linux script - version '+current_ver)
print ('Using output type',CUE)
print ('Output file is "', outputfile)
